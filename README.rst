

.. image:: https://img.shields.io/gitlab/pipeline/nannos/docker/master?style=for-the-badge
   :target: https://gitlab.com/nannos/docker/commits/master
   :alt: pipeline status

.. image:: https://img.shields.io/docker/v/benvial/nannos?color=8678bd&label=dockerhub&logo=docker&logoColor=white&style=for-the-badge
  :target: https://hub.docker.com/repository/docker/benvial/nannos
  :alt: dockerhub

.. image:: https://img.shields.io/docker/pulls/benvial/nannos?color=8678bd&style=for-the-badge
  :alt: docker pulls

.. image:: https://img.shields.io/docker/image-size/benvial/nannos?color=8678bd&style=for-the-badge
  :alt: docker image size
  
.. image:: https://img.shields.io/badge/license-GPLv3-blue?color=bb798f&style=for-the-badge
  :target: https://gitlab.com/nannos/docker/-/blob/master/LICENCE.txt
  :alt: license



Docker for Nannos
=================

Container with the Nannos code.

To build 


.. code-block:: bash

   docker pull benvial/nannos

