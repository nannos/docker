FROM condaforge/mambaforge:latest

SHELL ["bash", "-c"]

RUN apt-get update -q && apt-get install -q -y --no-install-recommends \
    libgl1-mesa-dev \
    && apt-get clean && rm -rf /var/lib/apt/lists/*

RUN conda config --set always_yes yes --set changeps1 no \
    && wget https://gitlab.com/nannos/nannos/-/raw/master/environment.yml -q \
    && sed -i 's/name: nannos/name: base/g' environment.yml \
    && mamba env update -n base -f environment.yml \
    && rm -f environment.yml \
    && pip cache purge \
    && conda clean --tarballs --index-cache --packages \
    && find ${CONDA_DIR} -follow -type f -name '*.a' -delete \
    && find ${CONDA_DIR} -follow -type f -name '*.pyc' -delete \
    && find ${CONDA_DIR} -follow -type f -name '*.js.map' -delete